var application_root = __dirname,
	express = require('express'),
	path = require('path');
var mongojs = require('mongojs');
var databaseUrl = 'localhost:27017/wall';
var username= '';
var pswd = '';
var collections = ['users','logs'];
//var db = //require('./db.js');//
var db = mongojs.connect(databaseUrl,collections);
var app = express();
// Config

app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(application_root, "public")));
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});


app.get('/getallusers', function(req,res){
	res.header("Access-Control-Allow-Origin", "http://localhost");
	res.header("Access-Control-Allow-Methods", "GET, POST");
	db.users.find(function(err,users){
		if(err||!users) console.log("No users found");
		else 
		{
			res.writeHead(200,{'Content-Type': 'text\plain'});
			str='';
			users.forEach(	function(user){
							str= str+user.em+"\n";
							});
			res.end(str)
		}
	})
});

app.get('/',function(req,res){
			res.writeHead(200,{'Content-Type': 'text\plain'});
	res.end('The box at Wall');
})


app.listen(1337);

